# puppeteer-pseudostate

Proof-of-concept to demonstrate the ability to automate triggering of element pseudostate without triggering javascript (event-based) behavior.

This would allow checks like those suggested in https://github.com/dequelabs/axe-core/issues/809 where axe rules can be verified against interactive elements with (any combination of) `:focus, :hover, :active, :visited` states applied.

AFAICT, it's not possible to do this in an extension. It's normally only available to developers in the devtools, https://developers.google.com/web/updates/2015/05/triggering-of-pseudo-classes.

```js
npm i
node ./index.js
```
