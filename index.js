// https://github.com/joelgriffith/browserless
// https://developers.google.com/web/updates/2018/01/devtools-without-devtools
// https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#targetcreatecdpsession
// https://chromedevtools.github.io/devtools-protocol/tot/CSS/#method-forcePseudoState
// https://gist.github.com/stephenmathieson/805a70e9f20d4e99c167ae1bc4ba9a31
const puppeteer = require('puppeteer');
const axe = require('axe-core');
const util = require('util');
const pluck = require('dlv');
// TODO
// const fastJson = require('fast-json-stringify');
const highlight = require('ansi-highlight');

(async () => {
  // console.log(await puppeteer.defaultArgs());
  const runAxe = `axe.run({runOnly: {type:'rule', values: ['color-contrast']}})`;
  const args = await puppeteer
    .defaultArgs()
    .filter(flag => flag !== '--enable-automation');
  const browser = await puppeteer.launch({
    headless: true,
    ignoreDefaultArgs: true,
    args
  });
  const page = await browser.newPage();
  await page.goto('https://www.gov.uk/contact/govuk');

  // get a baseline
  await page.evaluateHandle(axe.source);
  await displayResults('pseudostate normal', await page.evaluateHandle(runAxe));

  // collect all nodeIds we want to test
  const devtoolsProtocolClient = await page.target().createCDPSession();
  const root = await devtoolsProtocolClient.send('DOM.getDocument');
  const { nodeIds } = await devtoolsProtocolClient.send(
    'DOM.querySelectorAll',
    {
      nodeId: root.root.nodeId,
      selector: 'input, a'
    }
  );
  await devtoolsProtocolClient.send('CSS.enable');
  const states = ['focus', 'focus-within', 'active', 'hover', 'visited'];
  const allStates = [...kCombinations(states)];

  async function* scenarios() {
    const len = allStates.length;
    let i = 0;
    for (; i < len; i += 1) {
      const pseudostates = allStates[i];
      await Promise.all(
        nodeIds.map(async nodeId =>
          devtoolsProtocolClient.send('CSS.forcePseudoState', {
            nodeId: nodeId,
            forcedPseudoClasses: pseudostates
          })
        )
      );
      await displayResults(
        `pseudostate :${pseudostates.join(', :')}`,
        await page.evaluateHandle(runAxe)
      );
      yield pseudostates;
    }
  }

  const allScenarios = scenarios();
  let done = false;
  while (!done) {
    const result = await allScenarios.next();
    // TODO screenshots and diffs
    // if (result.value) {
    //   await page.screenshot({
    //     path: `./screenshots/${result.value.join('_')}.png`,
    //     fullPage: true
    //   });
    // }
    done = result.done;
  }

  await browser.close();
})();

async function displayResults(msg, handle) {
  const results = await handle.jsonValue();
  console.log(msg);
  if (!results.violations.length) {
    console.log('no violations');
    return;
  }
  console.log(
    highlight(
      util.inspect(
        results.violations
          .find(({ id }) => id === 'color-contrast')
          .nodes.map(node => {
            return {
              ...pluck(node, 'any.0.data'),
              target: node.target
            };
          }),
        { depth: null }
      )
    )
  );
}

function* kCombinations(items, limitK = items.length) {
  let k = 1;
  for (; k <= limitK; k += 1) {
    yield* combine(items, k);
  }
}

function* combine(items, k, continuation = {}) {
  const { data = [], start = 0, index = 0 } = continuation;
  if (index === k) {
    yield data.slice(0, index);
  }
  const end = items.length - 1;
  for (let i = start; i <= end && end - i + 1 >= k - index; i += 1) {
    data[index] = items[i];
    yield* combine(items, k, { data, start: i + 1, index: index + 1 });
  }
}

// TODO
// capture computed style
// modify pseudostate
// capture modified computed style
// compare computed styles for 3:1, border, outline, box-shadow (other?)
